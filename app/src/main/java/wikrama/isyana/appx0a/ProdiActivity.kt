package wikrama.isyana.appx0a

import android.app.Activity
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_prodi.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap

class ProdiActivity : AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert2 ->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate2 ->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete2 ->{
                queryInsertUpdateDelete("delete")
            }
        }
    }

    //lateinit var mediaHelper: MediaHelper
    //lateinit var mhsAdapter : AdapterDataMhs
    val activity = this@ProdiActivity
    lateinit var prodiAdapter : AdapterDataProdi
    var daftarProdi = mutableListOf<HashMap<String,String>>()
    val url4 = "http://192.168.1.18/kampus/show_data_prodi.php"
    val url5 = "http://192.169.1.18/app-x0a-web/pro_upd_del_ins_php"
    var pilihProdi = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prodi)

        //mediaHelper = MediaHelper(this)
        listProdi.layoutManager = LinearLayoutManager(this)
        //listMhs.layoutManager = LinearLayoutManager(this)
        listProdi.adapter = prodiAdapter
        //listMhs.adapter = mhsAdapter
        //prodiAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line, daftarProdi)
        //spProdi.adapter = prodiAdapter
        //spProdi.onItemSelectedListener = itemSelected
        //imgUpload.setOnClickListener(this)
        btnInsert2.setOnClickListener(this)
        btnUpdate2.setOnClickListener(this)
        btnDelete2.setOnClickListener(this)
        btnFind2.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataProdi(" ")
    }


    fun queryInsertUpdateDelete(mode:String){
        val request = object : StringRequest(Method.POST,"",
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil",Toast.LENGTH_LONG).show()
                    showDataProdi("")
                }else{
                    Toast.makeText(this,"Operasi Gagal",Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){


            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                //val nmFile = "DC"+SimpleDateFormat("yyyMMddHHmmss", Locale.getDefault())
                //   .format(Date())+".jpg"
                when(mode){
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("nim",edIdProdi.text.toString())
                        hm.put("nama",edNamaProdi.text.toString())
                    }
                    "update" -> {
                        hm.put("mode","update")
                        hm.put("nim",edIdProdi.text.toString())
                        hm.put("nama",edNamaProdi.text.toString())
                    }
                    "delete" -> {
                        hm.put("mode","delete")
                        hm.put("id_prodi",edIdProdi.text.toString())
                    }
                }
                return hm
            }
        }
    }

    fun showDataProdi(namaProdi : String){
        val request = object : StringRequest(Request.Method.POST,url4,
            Response.Listener { response ->
                daftarProdi.clear()
                val jsonArray = JSONArray(response)
                for (x in 0 ..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var pr = HashMap<String,String>()
                    pr.put("id_prodi",jsonObject.getString("id_prodi"))
                    pr.put("nama_prodi",jsonObject.getString("nama_prodi"))
                    daftarProdi.add(pr)
                }
                prodiAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            //override fun getParams(): MutableMap<String, String> {
               // val hm = HashMap<String,String>()
               // hm.put("nama_prodi",namaProdi)
                //return hm
            //}
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}

