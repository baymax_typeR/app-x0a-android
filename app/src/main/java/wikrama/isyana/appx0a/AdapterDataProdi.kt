package wikrama.isyana.appx0a

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_prodi.*
import kotlinx.android.synthetic.main.row_prodi.*

class AdapterDataProdi(val dataProdi : List<HashMap<String, String>>,
                     val prodiActivity: ProdiActivity) : //new
    RecyclerView.Adapter<AdapterDataProdi.HolderDataProdi>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataProdi.HolderDataProdi {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_prodi,p0,false)
        return HolderDataProdi(v)
    }

    override fun getItemCount(): Int {
        return dataProdi.size
    }

    override fun onBindViewHolder(p0: AdapterDataProdi.HolderDataProdi, p1: Int) {
        val data = dataProdi.get(p1)
        p0.txIdProdi.setText(data.get("id_prodi"))
        p0.txNamaProdi.setText(data.get("nama_prodi"))
        if (p1.rem(2)==0){
            p0.cLayout.setBackgroundColor(Color.rgb(230,245,245))
        }
        else{
            p0.cLayout.setBackgroundColor(Color.rgb(255,255,245))
        }
        p0.cLayout.setOnClickListener(View.OnClickListener {
            prodiActivity.txNamaProdi.setText(data.get("nama_prodi"))
        })
        //p0.txProdi.setText(data.get("nama_prodi"))

        //if(!data.get("url").equals(""))
        //    Picasso.get().load(data.get("url")).into(p0.photo);
        //p0.txTglLhr.setText(data.get("tgllahir"))
        //p0.txGender.setText(data.get("gender"))
        //p0.txAlamat.setText(data.get("alamat"))

    }

    class HolderDataProdi(v : View) : RecyclerView.ViewHolder(v) {
        //val txNim = v.findViewById<TextView>(R.id.txNim)
        //val txNama = v.findViewById<TextView>(R.id.txNama)
        //val txProdi = v.findViewById<TextView>(R.id.txProdi)
        //val photo = v.findViewById<ImageView>(R.id.imageView)
        //val txTglLhr = v.findViewById<TextView>(R.id.txTglLhr)
        //val txGender = v.findViewById<TextView>(R.id.txGender)
        //val txAlamat = v.findViewById<TextView>(R.id.txAlamat)
        //val cLayout = v.findViewById<ConstraintLayout>(R.id.clayout) //new
        val txIdProdi = v.findViewById<TextView>(R.id.txIdProdi)
        val txNamaProdi = v.findViewById<TextView>(R.id.txNamaProdi)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.clayout)
    }
}