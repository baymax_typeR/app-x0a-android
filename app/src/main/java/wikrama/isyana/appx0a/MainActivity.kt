package wikrama.isyana.appx0a

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnToMhs ->{
                var m = Intent(this, MahasiswaActivity::class.java)
                startActivity(m)
                true
            }
            R.id.btnToProdi ->{
                var p = Intent(this, ProdiActivity::class.java)
                startActivity(p)
                true
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnToMhs.setOnClickListener(this)
        btnToProdi.setOnClickListener(this)
    }
}